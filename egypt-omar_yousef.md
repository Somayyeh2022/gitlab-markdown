# Omar Farouk Yousef

## Architecture

[Photo of Me](https://drive.google.com/file/d/1Xjwy8oRUtM3wVRQSHf_ng9-E_b4wVmb-/view?usp=sharing)

Hello everyone! :wave:

My name is **Omar** and my home **country** is 

![Egyptian Flag](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_Egypt.svg/2560px-Flag_of_Egypt.svg.png)

If you are not familiar with the flag (or even if you are), then the following will probably help/assure. 

![Sphinx and the Pyramids](https://cdn.kimkim.com/files/a/content_articles/featured_photos/d21b085ef2ce77c4331c462d385aed66315ae019/big-c0028d755da3df7d7160a6015869431d.jpg)

My academic background is from **Architecture**.

![Architecture](http://tarmescapital.com/wp-content/uploads/2017/09/HealthcareDesignArchitecture05.jpg)

Here is a [picture](https://drive.google.com/file/d/17kvqzyluAOlWL_7bhhm3jysmn8hQGK_k/view?usp=sharing) of the most architectural project that I am proud of. (It is very **impressive**, highly recommended). :wink:

And here is another [picture](https://drive.google.com/file/d/1ytyqU-ftXB1Z4qyI-a9vqheiZc54E_U3/view?usp=sharing) of me standing beside the model (because it was relatively huge).

I joined this program imagining I would work in a team of 4 (each from one major) to create a robot which would use intelligence to theoretically build the project that the team would agree upon. Turns out that the first semester is probably goint to be mostly coding, which is something I can't really say I'm a fan of! :new_moon_with_face:

But I guess I can adapt, since it's also healthy to get out of your comfort zone from time to time. :relieved:

I have several hobbies.

#### 1. Music

I like listening to music.

![Music](https://images.macrumors.com/t/vMbr05RQ60tz7V_zS5UEO9SbGR0=/1600x900/smart/article-new/2018/05/apple-music-note.jpg)

My favorite bands would be **Arctic Monkeys** and **The Neighbourhood**.

![Arctic Monkeys](https://m.media-amazon.com/images/I/71-Y-3usHkL._SL1500_.jpg)

![The Neighbourhood](https://m.media-amazon.com/images/I/71ILxrYw3tL._SL1500_.jpg)

#### 2. Football

I like to watch football matches, and in my free time I would like to play as well.

![Football](https://www.gatwickunited.com/wp-content/uploads/2020/03/football-ball-size-5.jpg)

My favorite teams are **Liverpool** and **Juventus**.

![Liverpool](https://upload.wikimedia.org/wikipedia/de/thumb/0/0a/FC_Liverpool.svg/1200px-FC_Liverpool.svg.png)

![Juventus](https://cdn.vox-cdn.com/thumbor/lN4ga-jeQIii_9k7XAh_hI1WAVE=/0x0:934x622/1200x0/filters:focal(0x0:934x622):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/7818751/juventus.jpg)

But the most important one for me, is the one I locally support. That would be the Egyptian **Al Ahly**.

![Al Ahly](https://upload.wikimedia.org/wikipedia/en/8/8c/Al_Ahly_SC_logo.png)

My favorite players of all time would be **Andrea Pirlo**, **Lionel Messi**, **Fernando Torres**, and **Xabi Alonso**.

![Andrea Pirlo](https://www.fussballeuropa.com/images/xtra/andrea-pirlo-juventus-turin.jpg)

![Lionel Messi](https://www.fussballeuropa.com/images/xtra/lionel-messi-2021-36.jpg)

![Fernando Torres](http://images1.fanpop.com/images/image_uploads/Fernando-Torres-liverpool-fc-1154521_1920_1295.jpg)

![Xabi Alonso](https://imgresizer.eurosport.com/unsafe/1200x1200/smart/filters:format(jpeg)/origin-imgresizer.eurosport.com/2020/08/29/2873532-59178648-2560-1440.jpg)

#### 3. Swimming

In very hot summer days, I would love to get a dip in a **swimming pool**, or even go to the **beach**! :heart_eyes:

![Pool](https://www.happy-pool.com/wp-content/uploads/infinity-pool-mit-landschaft.jpg)

![Beach](https://vcdn.visit.guide/uploads/general_images/original/3879836371618685166.jpg)

#### 4. Tennis

Every once in a while, I like to play a game of tennis. I'm not that good at it, but I really do enjoy it.

![Tennis](https://montevallofalcons.com/images/2022/3/11/istockphoto_1171084311_170667a.jpg)

#### 5. Travelling

I really love travelling and seeing new land and cultures. My dream is to live for at least a year across different countries in South America.

Here's a list of the countries I have in mind:

* Argentina
* Bolivia
* Brazil
* Chile
* Colombia
* Ecuador
* Paraguay
* Peru
* Uruguay
* Venezuela

This is a [picture](https://drive.google.com/file/d/1fIS6tsGgJoMGTEEpdVg7WQX_cQSBmOEK/view?usp=sharing) of my bestfriend Shady. We have been friends since kindergarten (17 years now :smirk:). We applied and got accepted together to this program. Luckily, I don't have to tell you more about him since you can get to know more from his own repository.

Here are my [Facebook](https://www.facebook.com/omarrfaroukk) and [Instagram](https://www.instagram.com/omarrfaroukk/) profiles, feel free to take a closer look if you're interested.

Looking forward to getting to know you all!



# Markdown Tutorial

## This is a Title

### This is a Subtitle

This is what I want to say.  

It is indeed.

Writing in MarkDown is _not_ that hard!

I **will** complete these lessons!

_"Of course,"_ she whispered. Then, she shouted: **"All I need is a little moxie!"**

If you're thinking to yourself, **_This is unbelievable_**, you'd probably be right.

# Header One

## Header Two

### Header Three

#### Header Four

##### Header Five

###### Header Six

#### Colombian Symbolism in One Hundred Years of Solitude

Here's some words about the book _One Hundred Years..._

[Search for it.](http://www.google.com)

[You're **really, really** going to want to see this.](http://www.dailykitten.com)

#### The Latest News from [the BBC](http://www.bbc.com/news)

!["A pretty tiger"](https://upload.wikimedia.org/wikipedia/commons/5/56/Tiger.50.jpg)

![Black cat](https://upload.wikimedia.org/wikipedia/commons/a/a3/81_INF_DIV_SSI.jpg)

![Orange cat](http://icons.iconarchive.com/icons/google/noto-emoji-animals-nature/256/22221-cat-icon.png)
