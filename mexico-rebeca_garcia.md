# About me
## Who am I? ![who,who](https://melschwartz.com/wp-content/uploads/2021/05/H3-1.png)
> * Name: Rebeca García
> * Age: 28 years old
> * Nationality: Mexican 

I have a bachelor degree in **Architecture** and I worked one year in a _civil engineering office_ and 2 more years in a _design studio_ in Mexico City. Although in both jobs I had great professional experiences, I think that construction is very backward concerning technological progress; specifically in Mexico, the innovation processes are very slow or even null.  
#### My hobbies



