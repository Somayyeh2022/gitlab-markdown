# Concept: Make it easer to build their own house with AR

I found some interesting woord bricks for DIY house. Inspired by [Digital Building Instructions App of LEGO](https://www.youtube.com/watch?v=fAt1UfQSSJk), AR may make the DIY house assembly process much easier and fun.

### Some Thoughts
- embed water supply in ground layer
- embed electrical wires/ sensors in wall/roof
- door/window module
- simple design editor for customer
- cost/structure analysis
- robots to automate assembly
- Roof module without crane
- Treehouse for Kids

### Referrences
- [Gablok](https://gablok.com/en/)  
  Gablok was founded in Belgium in 2019 with a single objective: to enable anyone to build their own construction project.  
  [![Gablok - Timelapse Montage Maison Rapide avec blocs de construction isolé Gablok](https://img.youtube.com/vi/c7MOpY6mi90/default.jpg)](https://youtu.be/c7MOpY6mi90)

- [Brikawood](https://www.brikawood-ecologie.fr/)  
  A system without nails, screws or glue, like an intuitive and logical game  
  [![Montage d'une Maison Brikawood](https://img.youtube.com/vi/ierqMW_FxfE/default.jpg)](https://youtu.be/ierqMW_FxfE)

- The Future of Construction with AR  
  [![The Future of Construction with Augmented Reality](https://img.youtube.com/vi/ppTkOHKCBzk/default.jpg)](https://youtu.be/ppTkOHKCBzk)

- From Grasshopper to QuickPreview in iOS
  * [part1: Making use of the USDZ file format](https://medium.com/@jorgesalvador.arqdev/from-grasshopper-to-quickpreview-in-ios-making-use-of-the-usdz-file-format-part-one-ce8ac6b7ec68)
  * [part2: Using Unity](https://medium.com/@jorgesalvador.arqdev/from-grasshopper-to-augmented-reality-in-ios-using-unity-part-2-c68bf137fcca)

- [Fologram](https://fologram.com/)
- [Mindesk](https://mindeskvr.com/)

### Mindmap 	
```plantuml
@startmindmap
*[#Orange] 3D Model
	*[#lightgreen] AR Layer
		*[#lightblue] Site calibrate
			* Computer Vision
		*[#lightblue] Next Block Selection
			* Mistake Warning
		*[#lightblue] Assembly Check
			* Correction
		*[#lightblue] Record Process
			* Restore Process
			* Time lapse
	*[#lightgreen] Assembly Planning
		*[#lightblue] Real-time Changing
@endmindmap
```

[Miro Board](https://miro.com/app/board/uXjVO9EW93g=/?moveToWidget=3458764523412024127&cot=14)

# My experience and thoughts about CR Master

I'm Software Engineer with several years of work experience in Game and mobile software development. This is my 3rd semester of CR Master, although I didn't have much time on it last year. I learned some concepts about parametic design, cloud robot control, digital twins.

What I should remind others first is that CR Master is a research oriented course and would update its main contents every semester. For the DDP, it has diferent topics for diferent semester according to progress. Last year, you could choose IoT(rpi4) or Cloud Robot Control(with grasshoper). And this semester, DDP devide introduce us to the real problems on Construcion. Both are **NOT** exact the three projects as the scope describes.

I had been confused in my previous DDP and did not know what to do for the class in the first serveral weeks as I did have any sense for concept founding and design as a computer expert. In the middle, as I did not have enough time on DDP, I had a lot of stress to push my progress and catch up with the course. There were indeed many problems to solve, which may means impractical to deploy to physical world. I chose the easy way "give up and try next time when I'm ready".

For my 2nd semester, I focused on robotics related cources and the CR Letures Series, although again I still didn't have much time to learn due to the slow application process for the visa of my child. When I did not concern about the grade, I found I could learn better and concentrate on the parts that interest me the most. Considering the complexity of CR Master, I think the best strategy is finding a part that insterest you the most and then extend your vison and enjoy the challenge. If some part seems too hard for you, double the time or leave it. It would become easier when you learn more about other parts.

I am also worried about the internship requirements before the start of master thesis. As I need to take care of my child, I may not be able to travel to another city just for several weeks. A remote intership in software/planning manufacturer in the construction industry would suit my case. Or I'd better consider moving to another city for the intership and finishing master thesis there. 

Now I'm developing my idea about construction automation, integrating robotics, digital twin, AR. 

A journey to build a cozy homestead, easy customized and affordable for everyone
- :house: for shelter
- Garden for :bouquet: and :herb:
- Field for food :corn: vegetables :tomato: fruits :apple:
- Woods :evergreen_tree: for resources & advanture

Todo List / Build Blocks
- [x] Eclipse Zenoh for IoT Network
- [ ] Design and simulate mobile robot in Rhino & Grasshopper
- [ ] [Digital Twins with Unity](https://unity.com/solutions/digital-twin)
- [ ] [Eurobot](https://www.eurobot.org/)
- [ ] Algorithms for navigation and tasks on construcion site (with digital twins/ metaverse)
- [ ] AR: [Unity Refect](https://unity.com/products/unity-reflect) / [Trimble Connect AR](https://fieldtech.trimble.com/en/product/trimble-connect-ar)
- [ ] Easy custom wood brick house [gablok](https://gablok.be/en/) [Brikawood](https://www.brikawood-ecologie.fr/)
- [ ] Experiments with Bevy (ECS Game Engine in Rust, Open Source and free)

With the booming development of mobile robot, we may try to think how to optimize the construction process with collaboration with Spot Arm.

![Spot Arm](/assets/BOSTON-DYNAMICS-ARM-CLIP-CONSOLIDATED.2020-10-21-13_04_24.gif)


This interesting combination may be one of construction workers in future.
![The Youbionic One and Boston Dynamics mechanical centaur](/assets/Youbionic_One_and_Boston_Dynamics_mechanical_centaur.jpg)

If construcion work needs more stability ,load and flexibility, 6-legged hexapod robot platform may be also worthwhile to explore.

[Zee: Hexapod robot for remote inspection](https://research.csiro.au/robotics/zee/)
![Zee](https://i0.wp.com/research.csiro.au/robotics/wp-content/uploads/sites/96/2017/03/Zee-outdoor-walking.jpg)

[Mantis is the World’s Largest Rideable Hexapod Robot](https://www.techeblog.com/mantis-largest-rideable-hexapod-robot/)

![Mantis](https://media.techeblog.com/images/mantis-largest-hexapod-robot.jpg)


Tachikoma led me to the world of robots.

![tachikoma from ghost in the shell](/assets/ghost-in-the-shell-robot-spirits-action-figure-side-ghost-tachikoma-sac-2nd-gig-sac-2045.jpeg)

---
# [Online Markdown tutorial](https://www.markdowntutorial.com/) Quick Overview

### _Italics_ and **Bold**

If you're **thinking** to yourself, **_This is unbelievable_**, you'd _probably_ be right.

## Headers
#### Colombian Symbolism in _One Hundred Years of Solitude_

Here's some words about the book _One Hundred Years..._.

## Links and Referrence

[You're **really, really** going to want to see this.](www.dailykitten.com)

Do you want to [see something fun][a fun place]?

Well, do I have [the website for you][another fun place]!

[a fun place]: www.zombo.com
[another fun place]: www.stumbleupon.com

## Images

Tiger with custom width  
<img src="https://upload.wikimedia.org/wikipedia/commons/5/56/Tiger.50.jpg" alt="drawing" width="100"/>

![Black cat][Black]

![Orange cat][Orange]

[Black]: https://upload.wikimedia.org/wikipedia/commons/a/a3/81_INF_DIV_SSI.jpg
[Orange]: https://s36537.pcdn.co/wp-content/uploads/2018/01/Orange-tabby-cat-sleeping-with-eyes-closed.jpg

## Blockquotes
> He left her quickly, fearing that her intimacy might turn to jibing and wishing to be out of the way before she offered her ware to another, a tourist from England or a student of Trinity. Grafton Street, along which he walked, prolonged that moment of discouraged poverty. In the roadway at the head of the street a slab was set to the memory of Wolfe Tone and he remembered having been present with his father at its laying. He remembered with bitterness that scene of tawdry tribute. There were four French delegates in a brake and one, a plump smiling young man, held, wedged on a stick, a card on which were printed the words: _VIVE L'IRLANDE_!

## Lists
* Flour
* Cheese
* Tomatoes

1. Cut the cheese
1. Slice the tomatoes
1. Rub the tomatoes in flour

* Calculus
  * A professor
  * Has no hair
  * Often wears green
* Castafiore
  * An opera singer
  * Has white hair
  * Is possibly mentally unwell

 1. Cut the cheese  
    Make sure that the cheese is cut into little triangles.

 1. Slice the tomatoes  
    Be careful when holding the knife.  
    For more help on tomato slicing, see Thomas Jefferson's seminal essay _Tom Ate Those_.


## Paragraphs
double space at the end of line would make a soft break for a Paragraph

---
# [kramdown](https://kramdown.gettalong.org/quickref.html)



a small application writen in Rust to say hello.   
~~~rust  

use ferris_says::say; // from the previous step
use std::io::{stdout, BufWriter};

fn main() {
    let stdout = stdout();
    let message = String::from("Hello fellow Rustaceans!");
    let width = message.chars().count();

    let mut writer = BufWriter::new(stdout.lock());
    say(message.as_bytes(), width, &mut writer).unwrap();
}


~~~
which has fallowing ouput
~~~sh
----------------------------
< Hello fellow Rustaceans! >
----------------------------
              \
               \
                 _~^~^~_
             \) /  o o  \ (/
               '_   -   _'
               / '-----' \


~~~



