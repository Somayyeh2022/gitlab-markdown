---
title: About Front Matter
example:
  language: yaml
---

Introduction
==========

I am _Miguel Gutierrez_ from [Colombia](https://www.google.com/search?q=colombia&rlz=1C1ALOY_esCO999CO999&source=lnms&tbm=isch&sa=X&ved=2ahUKEwir76fxj5H3AhWiSTABHRosBjoQ_AUoAnoECAIQBA&biw=1024&bih=657&dpr=1). I am a civil engineer with a specialization and experience in the field of: _**Structural Design.**_ For example I have worked in design of Steel and Concrete buildings.  

![Steel][Example1]
![Concrete][Example2]

Example of quote, Walt Whitman said: 
>**"Keep your face always toward the sunshine - and shadows will fall behind you."**


```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

- `#F00sdfsdfs`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`


- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
   1. [ ] Sub-task 1
   1. [x] Sub-task 2

| Left Aligned | Centered | Right Aligned |
| :---         | :---:    | ---:          |
| Cell 1       | Cell 2   | Cell 3        |
| Cell 4       | Cell 5   | Cell 6        |



~~~ plantuml

@startmindmap
* Debian
** Ubuntu
*** Linux Mint
*** Kubuntu
*** Lubuntu
*** KDE Neon
** LMDE
** SolydXK
** SteamOS
** Raspbian with a very long name
*** <s>Raspmbc</s> => OSMC
*** <s>Raspyfi</s> => Volumio
@endmindmap

~~~


[Example1]: https://4.imimg.com/data4/GJ/LX/MY-13070075/steel-buildings-500x500.jpeg

[Example2]: https://static.turbosquid.com/Preview/2016/09/17__06_45_51/1.jpg393399E8-009C-494F-A96D-623FB0E0F193Original.jpg


